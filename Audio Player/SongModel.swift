//
//  SongModel.swift
//  Audio Player
//
//  Created by Mohsin Ahmed on 4/2/17.
//  Copyright © 2017 Mohsin Ahmed. All rights reserved.
//

import UIKit

class SongModel: NSObject {
    
    var name:String?
    var artist:String?
    var album:String?
    var genre:String?
    var image_name:String?
    
    init(name:String, artist:String, album:String, genre:String, image_name:String) {
        
        self.name = name
        self.artist = artist
        self.album = album
        self.genre = genre
        self.image_name = image_name
    }

}
