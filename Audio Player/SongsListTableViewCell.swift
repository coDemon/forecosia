//
//  SongsListTableViewCell.swift
//  Audio Player
//
//  Created by Mohsin Ahmed on 4/2/17.
//  Copyright © 2017 Mohsin Ahmed. All rights reserved.
//

import UIKit

class SongsListTableViewCell: UITableViewCell {

    @IBOutlet weak var lbl_SongName: UILabel!
    @IBOutlet weak var img_albumArt: UIImageView!
    
    var fileName : String = ""
    var cellRowIndex: Int = 0
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
    }


}
