//
//  AudioPlayerHandler.swift
//  Audio Player
//
//  Created by Mohsin Ahmed on 4/2/17.
//  Copyright © 2017 Mohsin Ahmed. All rights reserved.
//

import UIKit

class AudioPlayerHandler: NSObject {
    
    //MARK: Shared Instance
    static let sharedInstance: AudioPlayerHandler = AudioPlayerHandler()
    
    var modelsArray: [SongModel] = []
    
    func parseDataAndPopulateModels() {
        
        let fileName = Bundle.main.path(forResource: "SongsData", ofType: ".plist")
        let data = NSArray(contentsOfFile: fileName!) as! [[String:AnyObject]]
        
        for item in data {
            
            let name = item["name"] as! String
            let artist = item["artist"] as! String
            let album = item["album"] as! String
            let genre = item["genre"] as! String
            let imgName = item["image_art_name"] as! String
            
            let model = SongModel.init(name: name, artist: artist, album: album, genre: genre, image_name: imgName)
            modelsArray.append(model)
        }
    }
    
    func getSongModelForSongName(name:String) -> SongModel {
    
        var modelNeeded:SongModel?
        
        for model in modelsArray {
            
            if model.name == name {
                
                modelNeeded = model
                break
            }
        }
        
        return modelNeeded!
    }
}
