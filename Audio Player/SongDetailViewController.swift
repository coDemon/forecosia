//
//  SongDetailViewController.swift
//  Audio Player
//
//  Created by Mohsin Ahmed on 4/2/17.
//  Copyright © 2017 Bilal Hussain. All rights reserved.
//

import UIKit
import AVFoundation

class SongDetailViewController: UIViewController {
    
    @IBOutlet weak var progressBarSlider: UISlider!
    @IBOutlet weak var songAlbumArt: UIImageView!
    @IBOutlet weak var previousSongBtn: UIButton!
    @IBOutlet weak var currentSongBtn: UIButton!
    @IBOutlet weak var nextSongBtn: UIButton!
    @IBOutlet weak var songName: UILabel!
    @IBOutlet weak var progressTime: UILabel!
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var albumName: UILabel!
    @IBOutlet weak var genre: UILabel!
    
    var songModel:SongModel!
    var audioPlayer:AVAudioPlayer!
    var timer: DispatchSourceTimer?
    var songIndex:Int = 0
    var songsArray:[String] = []
  
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        setSongDetails()
        self.startTimer()
        self.navigationController?.navigationBar.isHidden = false
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        
    }
    
    func setSongDetails() {
        
        let image = UIImage.init(named: (songModel?.image_name)!)
        songAlbumArt.image = image
        songAlbumArt.layer.borderWidth = 2
        songAlbumArt.layer.borderColor = UIColor.orange.cgColor
        
        songName.text = songModel.name
        artistName.text = songModel.artist
        albumName.text = songModel.name
        genre.text = songModel.genre
        progressTime.text = "00:00"
        
        progressBarSlider.maximumValue = Float(audioPlayer.duration)
        progressBarSlider.value = 0.0
    }
    
    @IBAction func sliderMoved(_ sender: UISlider) {
        audioPlayer.currentTime = TimeInterval(progressBarSlider.value)
    }
    
    @IBAction func previousBtnAction(_ sender: UIButton) {

        if songIndex != 0
        {
            let resource = songsArray[songIndex-1]
            songModel = AudioPlayerHandler.sharedInstance.getSongModelForSongName(name: resource)
            setAudioPlayerWithMusic(fileName: resource, exten: ".mp3")
            songIndex -= 1
            
            setSongDetails()
        }
        else
        {
            
        }
    }

    @IBAction func currentBtnAction(_ sender: UIButton) {
        
        if !audioPlayer.isPlaying {
            
            audioPlayer.play()
            currentSongBtn.titleLabel?.text = "play"
            
        } else {
            
            audioPlayer.pause()
            currentSongBtn.titleLabel?.text = "pause"
            self.stopTimer()
        }
    }
    
    @IBAction func nextBtnAction(_ sender: UIButton) {
     
        if songIndex < songsArray.count-1
        {
            audioPlayer.stop()
            let resource = songsArray[songIndex+1]
            songModel = AudioPlayerHandler.sharedInstance.getSongModelForSongName(name: resource)
            setAudioPlayerWithMusic(fileName: resource, exten: ".mp3")
            songIndex += 1
            
            setSongDetails()
        }
        else
        {
            
        }
    }
    
    @IBAction func backBarBtnPressed(_ sender: Any) {
        
        self.performSegue(withIdentifier: "songList", sender: self)
    }
    
    func setAudioPlayerWithMusic(fileName:String, exten:String) {
        
        do {
            if audioPlayer.isPlaying {
                audioPlayer.stop()
            }
            
            audioPlayer = try AVAudioPlayer(contentsOf: URL.init(fileURLWithPath: Bundle.main.path(forResource: fileName, ofType: exten)!))
            audioPlayer.prepareToPlay()
            audioPlayer.play()
            startTimer()
            //Set Audio session for play at back
            let audioSession = AVAudioSession.sharedInstance()
            do {
                
                try audioSession.setCategory(AVAudioSessionCategoryPlayback)
                
            } catch {
                
            }
            
        }catch{
            print("Error in palying")
        }
    }
    
    func updateSlider() {
        
        let time = Float(audioPlayer.currentTime)
        print("\(time)")
        progressBarSlider.value = time
        
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        let timeString = String(format:"%02i:%02i", minutes, seconds)
    
        progressTime.text = timeString
    }
    
    func startTimer() {
        
        let queue = DispatchQueue(label: "com.domain.app.timer")
        timer = DispatchSource.makeTimerSource(queue: queue)
        timer!.scheduleRepeating(deadline: .now(), interval: .seconds(2))
        timer!.setEventHandler { [weak self] in
            
            self?.updateSlider()
        }
        timer!.resume()
    }
    
    func stopTimer() {
        
        progressTime.text = "00:00"
        timer?.cancel()
        timer = nil
    }
    
    deinit {
        
        self.stopTimer()
    }

}
