//
//  ViewController.swift
//  Audio Player
//
//  Created by Mohsin Ahmed on 4/2/17.
//  Copyright © 2017 Mohsin Ahmed. All rights reserved.
//

import UIKit
import AVFoundation


class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var songlistTableView: UITableView!
    
    var audioPlayer = AVAudioPlayer()
    
    var songsListArray:[String] = []
    var songsThumbsArray:[String] = []
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        gettingSongNames()
        self.navigationController?.navigationBar.isHidden = false
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
    }


    func setAudioPlayerWithMusic(fileName:String, exten:String) {
        
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: URL.init(fileURLWithPath: Bundle.main.path(forResource: fileName, ofType: exten)!))
            audioPlayer.prepareToPlay()
            audioPlayer.play()
            
            //Set Audio session for play at back
            let audioSession = AVAudioSession.sharedInstance()
            do {
                
                try audioSession.setCategory(AVAudioSessionCategoryPlayback)
                
            } catch {
                
            }
            
        }catch{
            print("Error in palying")
        }
    }
 
    
    //MARK :- Table view Delegate and Datasource
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return songsListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "listCell", for: indexPath) as! SongsListTableViewCell
        
        cell.lbl_SongName.text = songsListArray[indexPath.row]
        cell.fileName = songsListArray[indexPath.row]
        
        let image = UIImage.init(named: songsThumbsArray[indexPath.row])
        cell.img_albumArt.image = image
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let resource = songsListArray[indexPath.row]
        let cell:SongsListTableViewCell = tableView.cellForRow(at: indexPath) as! SongsListTableViewCell
        cell.cellRowIndex = indexPath.row
        
        self.setAudioPlayerWithMusic(fileName: resource, exten: ".mp3")
        self.performSegue(withIdentifier: "showSongDetail", sender: cell)
    }
    
    
    func gettingSongNames() {
        
        let folderURL = URL(fileURLWithPath:Bundle.main.resourcePath!)
        
        do {
            let songPath = try FileManager.default.contentsOfDirectory(at: folderURL, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
            
            //loop through the found urls
            for song in songPath {
                
                var mySong = song.absoluteString
                
                if mySong.contains(".mp3") {
                    
                    let findString = mySong.components(separatedBy: "/")
                    mySong = findString[findString.count-1]
                    mySong = mySong.replacingOccurrences(of: "%20", with: " ")
                    mySong = mySong.replacingOccurrences(of: ".mp3", with: "")
                    songsListArray.append(mySong)
                    
                } else if mySong.contains(".jpeg") {
                    
                    let findString = mySong.components(separatedBy: "/")
                    mySong = findString[findString.count-1]
                    mySong = mySong.replacingOccurrences(of: ".jpeg", with: "")
                    songsThumbsArray.append(mySong)
                }
            
            }
            
            songlistTableView.reloadData()
        }
        catch
        {
            print ("ERROR")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == "showSongDetail") {
            
            let nav = segue.destination as! UINavigationController
            let cell = sender as! SongsListTableViewCell
            let controller = nav.topViewController as! SongDetailViewController
            controller.songModel = AudioPlayerHandler.sharedInstance.getSongModelForSongName(name: cell.fileName)
            controller.audioPlayer = audioPlayer
            controller.songIndex = cell.cellRowIndex
            controller.songsArray = songsListArray
        }
        
    }
}

